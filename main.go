package main

import (
	// "fmt"
	"fmt"
	"net/http"

	//"go-fiber-test/models"

	"go-fiber-test/controllers/authcontroller"
	"go-fiber-test/db"

	//authcontroller "go-fiber-test/controllers/authcontroller"
	"log"

	//"net/http"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load()

	if err != nil {
		fmt.Println(err)
	}
}

func main() {

	//models.ConnectDatabase()
	db.ConnectDatabase()

	// app := fiber.New()
	// api := app.Group("/api")
	// book := api.Group("/book")

	// book.Get("/", controller.IndexBook)
	// book.Get("/:id", controller.ShowBook)
	// book.Post("/", controller.CreateBook)
	// book.Put("/:id", controller.UpdateBook)
	// book.Delete("/:id", controller.DeleteBook)

	// user := api.Group("/user")

	// user.Get("/", controller.IndexUser)
	// user.Get("/", controller.ShowUser)
	// user.Post("/", controller.CreateUser)
	// user.Put("/:id", controller.UpdateUser)
	// user.Delete("/:id", controller.DeleteUser)

	r := mux.NewRouter()
	http.HandleFunc("/login", authcontroller.Login)
	http.HandleFunc("/register", authcontroller.Register)
	http.HandleFunc("/logout", authcontroller.Logout)

	log.Fatal(http.ListenAndServe(":8080", r))

}
