package controller

import (
	"go-fiber-test/db"
	"go-fiber-test/models"
	"net/http"

	"gorm.io/gorm"

	"github.com/gofiber/fiber/v2"
)

func IndexUser(c *fiber.Ctx) error {
	var users []models.User
	db.DB.Find(&users)

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"user": users,
	})

}

func ShowUser(c *fiber.Ctx) error {

	id := c.Params("id")
	var user models.User
	if err := db.DB.First(&user, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.Status(http.StatusNotFound).JSON(fiber.Map{
				"message": "Data Not Found",
			})
		}

		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"message": "ServerError",
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"user": user,
	})
}

func CreateUser(c *fiber.Ctx) error {

	var user models.User
	if err := c.BodyParser(&user); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message1": err.Error(),
		})
	}

	if err := db.DB.Create(&user).Error; err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message22": err.Error(),
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"user": user,
	})
}

func UpdateUser(c *fiber.Ctx) error {

	id := c.Params("id")

	var user models.User
	if err := c.BodyParser(&user); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	if db.DB.Where("id = ?", id).Updates(&user).RowsAffected == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Data not found",
		})
	}

	return c.JSON(fiber.Map{
		"message": "Updated",
	})
}

func DeleteUser(c *fiber.Ctx) error {

	id := c.Params("id")

	var user models.User
	if db.DB.Delete(&user, id).RowsAffected == 0 {
		return c.Status(http.StatusNotFound).JSON(fiber.Map{
			"message": "Data not found",
		})
	}

	return c.JSON(fiber.Map{
		"message": "Deleted!",
	})
}
