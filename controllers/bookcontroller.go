package controller

import (
	"go-fiber-test/db"
	"go-fiber-test/models"
	"net/http"

	"gorm.io/gorm"

	"github.com/gofiber/fiber/v2"
)

func IndexBook(c *fiber.Ctx) error {
	var books []models.Book
	db.DB.Find(&books)

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"books": books,
	})

}

func ShowBook(c *fiber.Ctx) error {

	id := c.Params("id")
	var book models.Book
	if err := db.DB.First(&book, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.Status(http.StatusNotFound).JSON(fiber.Map{
				"message": "Data Not Found",
			})
		}

		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"message": "ServerError",
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"book": book,
	})
}

func CreateBook(c *fiber.Ctx) error {

	var book models.Book
	if err := c.BodyParser(&book); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	if err := db.DB.Create(&book).Error; err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"book": book,
	})
}

func UpdateBook(c *fiber.Ctx) error {

	id := c.Params("id")

	var book models.Book
	if err := c.BodyParser(&book); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	if db.DB.Where("id = ?", id).Updates(&book).RowsAffected == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Data not found",
		})
	}

	return c.JSON(fiber.Map{
		"message": "Updated",
	})
}

func DeleteBook(c *fiber.Ctx) error {

	id := c.Params("id")

	var book models.Book
	if db.DB.Delete(&book, id).RowsAffected == 0 {
		return c.Status(http.StatusNotFound).JSON(fiber.Map{
			"message": "Data not found",
		})
	}

	return c.JSON(fiber.Map{
		"message": "Deleted!",
	})
}
