package db

import (
	"fmt"
	"go-fiber-test/models"
	"log"
	"os"
	"strconv"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Connection interface{}

type conn struct {
	gorm *gorm.DB
}

var DB *gorm.DB

func ConnectDatabase() Connection {
	var c conn
	url := getRL()
	log.Println("URL : ", url)
	db, error := gorm.Open(mysql.Open(url))

	if error != nil {
		log.Panicln(error.Error())
	}
	db.AutoMigrate(models.User{})
	// db.AutoMigrate(models.Book{})

	DB = db
	return &c
}

func (c *conn) DB() *gorm.DB {
	return c.gorm
}

func getRL() string {

	port, err := strconv.Atoi(os.Getenv("DATABASE_PORT"))
	fmt.Println("dasdasd: ", port)

	db_user := os.Getenv("DATABASE_USER")
	db_pass := os.Getenv("DATABASE_PASS")
	db_host := os.Getenv("DATABASE_HOST")
	db_name := os.Getenv("DATABASE_NAME")

	fmt.Println("ERROR: ", err)
	fmt.Println("db_name: ", db_name)
	if err != nil {
		log.Println("error on load db port form env:", err.Error())
		port = 3306
	}
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
		db_user,
		db_pass,
		db_host,
		port,
		db_name,
	)
}
