package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

type conn struct {
	gorm *gorm.DB
}

func ConnectDatabase() {
	db, error := gorm.Open(mysql.Open("root:poppbsfs4za@tcp(localhost:3306)/go_restapi_fiber"))

	if error != nil {
		panic(error)
	}

	db.AutoMigrate(&User{})
	db.AutoMigrate(&Book{})
	DB = db
}
