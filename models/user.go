package models

import "time"

type User struct {
	Id        int64     `gorm:"primaryKey" json:"id"`
	Name      string    `gorm:"varchar(300)" json:"name"`
	Username  string    `gorm:"varchar(300)" json:"username"`
	Password  string    `gorm:"varchar(300)" json:"Password"`
	CreatedAt time.Time `json:"created_at" gorm:"column:created_at"`
	UpdateAt  time.Time `json:"updated_at" gorm:"column:updated_at"`
}
